//
//  ViewController.swift
//  HarryPT
//
//  Created by COTEMIG on 20/10/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Personagem: Decodable {
    let image: String
    let actor: String
    let name: String
}

class ViewController: UIViewController, UITableViewDataSource {
    var Lista: [Personagem] = []
    var userDefaults = UserDefaults.standard
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Lista.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = TabeView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MinhaCelula
        let coisa = Lista[indexPath.row]
        item.Imagem.kf.setImage(with: URL(string: coisa.image))
        item.Autor.text = coisa.actor
        item.Nome.text = coisa.name
        return item
    }
    
    
    
    
    func Personas(){
        AF.request("https://hp-api.onrender.com/api/characters").responseDecodable(of: [Personagem].self) { response in
            if let personagem = response.value {
                self.Lista = personagem
                print(personagem)
            }
            self.TabeView.reloadData()

}

    }
    @IBOutlet weak var TabeView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        TabeView.dataSource = self
        Personas()
    }
}

